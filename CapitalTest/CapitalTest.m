//
//  CapitalTest.m
//  CapitalTest
//
//  Created by Romans Karpelcevs on 1/11/13.
//
//

#import "CapitalTest.h"
#import "Geo.h"

@implementation CapitalTest

- (NSArray *)createPolygon
{
    NSMutableArray *polygon = [[NSMutableArray alloc] init];
    CLLocationCoordinate2D coor;

    coor = CLLocationCoordinate2DMake(0.0, 0.0);
    [polygon addObject:[NSValue valueWithBytes:&coor objCType:@encode(CLLocationCoordinate2D)]];
    coor = CLLocationCoordinate2DMake(4.0, 4.0);
    [polygon addObject:[NSValue valueWithBytes:&coor objCType:@encode(CLLocationCoordinate2D)]];
    coor = CLLocationCoordinate2DMake(0.0, 7.0);
    [polygon addObject:[NSValue valueWithBytes:&coor objCType:@encode(CLLocationCoordinate2D)]];
    coor = CLLocationCoordinate2DMake(-3.0, -4.0);
    [polygon addObject:[NSValue valueWithBytes:&coor objCType:@encode(CLLocationCoordinate2D)]];
    coor = CLLocationCoordinate2DMake(3.0, -4.0);
    [polygon addObject:[NSValue valueWithBytes:&coor objCType:@encode(CLLocationCoordinate2D)]];
    return polygon;
}

- (void)testAbsoluteHeadingFromZero
{
    CLLocationCoordinate2D current = CLLocationCoordinate2DMake(0.0, 0.0);
    CLLocationCoordinate2D target = CLLocationCoordinate2DMake(0.0, 0.0);

    CLLocationDirection heading = [Geo absoluteHeadingFrom:current to:target];
    STAssertEqualsWithAccuracy(heading, 0.0, 0.01, @"Target same as current.");

    target = CLLocationCoordinate2DMake(5.0, 0.0);
    heading = [Geo absoluteHeadingFrom:current to:target];
    STAssertEqualsWithAccuracy(heading, 0.0, 0.01, @"North from zero.");

    target = CLLocationCoordinate2DMake(5.0, 5.0);
    heading = [Geo absoluteHeadingFrom:current to:target];
    STAssertEqualsWithAccuracy(heading, 45.0, 0.01, @"Northeast from zero.");

    target = CLLocationCoordinate2DMake(0.0, 5.0);
    heading = [Geo absoluteHeadingFrom:current to:target];
    STAssertEqualsWithAccuracy(heading, 90.0, 0.01, @"East from zero.");

    target = CLLocationCoordinate2DMake(-5.0, 5.0);
    heading = [Geo absoluteHeadingFrom:current to:target];
    STAssertEqualsWithAccuracy(heading, 135.0, 0.01, @"Southeast from zero.");

    target = CLLocationCoordinate2DMake(-5.0, 0.0);
    heading = [Geo absoluteHeadingFrom:current to:target];
    STAssertEqualsWithAccuracy(heading, 180.0, 0.01, @"South from zero.");

    target = CLLocationCoordinate2DMake(-5.0, -5.0);
    heading = [Geo absoluteHeadingFrom:current to:target];
    STAssertEqualsWithAccuracy(heading, 225.0, 0.01, @"Southwest from zero.");

    target = CLLocationCoordinate2DMake(0.0, -5.0);
    heading = [Geo absoluteHeadingFrom:current to:target];
    STAssertEqualsWithAccuracy(heading, 270.0, 0.01, @"West from zero.");

    target = CLLocationCoordinate2DMake(5.0, -5.0);
    heading = [Geo absoluteHeadingFrom:current to:target];
    STAssertEqualsWithAccuracy(heading, 315.0, 0.01, @"Northwest from zero.");
}

- (void)testAbsoluteHeadingFromOtherLocation
{
    double locationOffset = -10.0;
    CLLocationCoordinate2D current = CLLocationCoordinate2DMake(locationOffset, locationOffset);
    CLLocationCoordinate2D target = CLLocationCoordinate2DMake(locationOffset, locationOffset);

    CLLocationDirection heading = [Geo absoluteHeadingFrom:current to:target];
    STAssertEqualsWithAccuracy(heading, 0.0, 0.01, @"Target same as current.");

    target = CLLocationCoordinate2DMake(locationOffset + 5.0, locationOffset + 0.0);
    heading = [Geo absoluteHeadingFrom:current to:target];
    STAssertEqualsWithAccuracy(heading, 0.0, 0.01, @"North from zero.");

    target = CLLocationCoordinate2DMake(locationOffset + 5.0, locationOffset + 5.0);
    heading = [Geo absoluteHeadingFrom:current to:target];
    STAssertEqualsWithAccuracy(heading, 45.0, 0.01, @"Northeast from zero.");

    target = CLLocationCoordinate2DMake(locationOffset + 0.0, locationOffset + 5.0);
    heading = [Geo absoluteHeadingFrom:current to:target];
    STAssertEqualsWithAccuracy(heading, 90.0, 0.01, @"East from zero.");

    target = CLLocationCoordinate2DMake(locationOffset + -5.0, locationOffset + 5.0);
    heading = [Geo absoluteHeadingFrom:current to:target];
    STAssertEqualsWithAccuracy(heading, 135.0, 0.01, @"Southeast from zero.");

    target = CLLocationCoordinate2DMake(locationOffset + -5.0, locationOffset + 0.0);
    heading = [Geo absoluteHeadingFrom:current to:target];
    STAssertEqualsWithAccuracy(heading, 180.0, 0.01, @"South from zero.");

    target = CLLocationCoordinate2DMake(locationOffset + -5.0, locationOffset + -5.0);
    heading = [Geo absoluteHeadingFrom:current to:target];
    STAssertEqualsWithAccuracy(heading, 225.0, 0.01, @"Southwest from zero.");

    target = CLLocationCoordinate2DMake(locationOffset + 0.0, locationOffset + -5.0);
    heading = [Geo absoluteHeadingFrom:current to:target];
    STAssertEqualsWithAccuracy(heading, 270.0, 0.01, @"West from zero.");

    target = CLLocationCoordinate2DMake(locationOffset + 5.0, locationOffset + -5.0);
    heading = [Geo absoluteHeadingFrom:current to:target];
    STAssertEqualsWithAccuracy(heading, 315.0, 0.01, @"Northwest from zero.");
}

- (void)doCustomHeadingTest:(double)offset
{
    CLLocationDirection current = offset + 0.0;
    CLLocationDirection target = current;
    CLLocationDirection heading = [Geo relativeHeadingWithCurrent:current andTarget:target];
    STAssertEqualsWithAccuracy(heading, 0.0, 0.01, @"Target same as current.");

    current = offset + 0.0;
    target = offset + 120.0;
    heading = [Geo relativeHeadingWithCurrent:current andTarget:target];
    STAssertEqualsWithAccuracy(heading, 120.0, 0.01, @"Positive half.");

    current = offset + 0.0;
    target = offset + 240.0;
    heading = [Geo relativeHeadingWithCurrent:current andTarget:target];
    STAssertEqualsWithAccuracy(heading, -120.0, 0.01, @"Negative half positive arg.");

    current = 360.0 * 3 + (offset + 0.0);
    target = 360.0 * 4 + (offset + 240.0);
    heading = [Geo relativeHeadingWithCurrent:current andTarget:target];
    STAssertEqualsWithAccuracy(heading, -120.0, 0.01, @"Negative half positive arg with period.");

    current = offset + 0.0;
    target = offset + -120.0;
    heading = [Geo relativeHeadingWithCurrent:current andTarget:target];
    STAssertEqualsWithAccuracy(heading, -120.0, 0.01, @"Negative half negative arg.");
}

- (void)testRelativeHeadingFromZero
{
    [self doCustomHeadingTest:0.0];
}

- (void)testRelativeHeadingFromPositive
{
    [self doCustomHeadingTest:111.2];
    [self doCustomHeadingTest:315.2];

    CLLocationDirection heading = [Geo relativeHeadingWithCurrent:310.0 andTarget:56.0];
    STAssertEqualsWithAccuracy(heading, 106.0, 0.01, @"Arbitrary result < -180");

    heading = [Geo relativeHeadingWithCurrent:56.0 andTarget:310.0];
    STAssertEqualsWithAccuracy(heading, -106.0, 0.01, @"Arbitrary result > 180");
}

- (void)testRelativeHeadingFromNegative
{
    [self doCustomHeadingTest:-85.3];
    [self doCustomHeadingTest:-270.3];
}

- (void)testDoubleEquality
{
    STAssertFalse(eq_double(0.001, 0.002), @"Really different numbers");
    STAssertFalse(eq_double(0.00000001, 0.00000002), @"Really different numbers");

    STAssertFalse(eq_double(0.0000000000001, 0.0000000000002), @"Barely different numbers");
    STAssertFalse(eq_double(0.1000000000001, 0.1000000000002), @"Barely different numbers");

    // Now one order greater
    STAssertTrue(eq_double(0.00000000000001, 0.00000000000002), @"Almost different numbers");
    STAssertTrue(eq_double(0.10000000000001, 0.10000000000002), @"Almost different numbers");
}

- (void)testPointInside
{
    NSArray *polygon = [self createPolygon];

    CLLocationCoordinate2D coor = CLLocationCoordinate2DMake(-1.0, -2.0);
    STAssertTrue([Geo isPoint:coor insideClosedPath:polygon], @"Inside main body");
    coor = CLLocationCoordinate2DMake(0.0, 6.0);
    STAssertTrue([Geo isPoint:coor insideClosedPath:polygon], @"Inside right point");
    coor = CLLocationCoordinate2DMake(3.8, 4.0);
    STAssertTrue([Geo isPoint:coor insideClosedPath:polygon], @"Inside top point");

    coor = CLLocationCoordinate2DMake(0.1, 0.0);
    STAssertFalse([Geo isPoint:coor insideClosedPath:polygon], @"Outside between slopes");
    coor = CLLocationCoordinate2DMake(2, 6.0);
    STAssertFalse([Geo isPoint:coor insideClosedPath:polygon], @"Outside right slope");
    coor = CLLocationCoordinate2DMake(-4, 4.0);
    STAssertFalse([Geo isPoint:coor insideClosedPath:polygon], @"Outside way far");

    coor = CLLocationCoordinate2DMake(0.0, 0.0);
    STAssertTrue([Geo isPoint:coor insideClosedPath:polygon], @"Inside on the egde");
    coor = CLLocationCoordinate2DMake(0.0 + DBL_EPSILON, 0.0);
    STAssertFalse([Geo isPoint:coor insideClosedPath:polygon], @"Outside on the egde");
}

- (void)testRelativeDistance
{
    CLLocationCoordinate2D loc1 = CLLocationCoordinate2DMake(3.0, 3.0);
    CLLocationCoordinate2D loc2 = CLLocationCoordinate2DMake(6.0, 7.0);

    STAssertEqualsWithAccuracy([Geo relativeDistanceFrom:loc1 to:loc2], 5.0, 0.01, @"Simple distance.");

    loc1 = CLLocationCoordinate2DMake(-1.0, -6.0);
    loc2 = CLLocationCoordinate2DMake(-4.0, -2.0);

    STAssertEqualsWithAccuracy([Geo relativeDistanceFrom:loc1 to:loc2], 5.0, 0.01, @"Simple distance in neg.");
}

- (void)testIntersectionBetweenLines
{
    CLLine line1 = CLLineMake(0, 0, 5, 3);
    CLLine line2 = CLLineMake(0, 3, 5, 6);

    CLLocationCoordinate2D intersection = [Geo intersectionBetween:line1 and:line2];
    STAssertEqualsWithAccuracy(intersection.longitude, (double)FLT_MAX, FLT_EPSILON, @"Parallel x");
    STAssertEqualsWithAccuracy(intersection.latitude, (double)FLT_MAX, FLT_EPSILON, @"Parallel y");

    line1 = CLLineMake(0, -2, 2, -2);
    line2 = CLLineMake(-3, 0, -3, -3);
    intersection = [Geo intersectionBetween:line1 and:line2];
    STAssertEqualsWithAccuracy(intersection.longitude, -3.0, FLT_EPSILON, @"Rectangular x");
    STAssertEqualsWithAccuracy(intersection.latitude, -2.0, FLT_EPSILON, @"Rectangular y");

    line1 = CLLineMake(-3, 0, 0, 5);
    line2 = CLLineMake(3, 0, 0, 5);
    intersection = [Geo intersectionBetween:line1 and:line2];
    STAssertEqualsWithAccuracy(intersection.longitude, 0.0, FLT_EPSILON, @"Diagonal x");
    STAssertEqualsWithAccuracy(intersection.latitude, 5.0, FLT_EPSILON, @"Diagonal y");
}

- (void)testIntersectionWithPolygon
{
    NSArray *polygon = [self createPolygon];
    CLLocationCoordinate2D coor = CLLocationCoordinate2DMake(2.0, 0.0);
    CLLocationDirection heading = 180.0;

    CLLocationCoordinate2D intersection = [Geo intersectionWithPolygon:polygon
                                                             fromPoint:coor andHeading:heading];
    STAssertEqualsWithAccuracy(intersection.longitude, 0.0, FLT_EPSILON, @"Straight down x");
    STAssertEqualsWithAccuracy(intersection.latitude, 0.0, FLT_EPSILON, @"Straight down y");

    heading = 90;
    intersection = [Geo intersectionWithPolygon:polygon fromPoint:coor andHeading:heading];
    STAssertEqualsWithAccuracy(intersection.longitude, 2.0, FLT_EPSILON, @"Straight right x");
    STAssertEqualsWithAccuracy(intersection.latitude, 2.0, FLT_EPSILON, @"Straight right y");

    heading = 0;
    intersection = [Geo intersectionWithPolygon:polygon fromPoint:coor andHeading:heading];
    STAssertEqualsWithAccuracy(intersection.longitude, (double)FLT_MAX, FLT_EPSILON, @"Straight up x");
    STAssertEqualsWithAccuracy(intersection.latitude, (double)FLT_MAX, FLT_EPSILON, @"Straight up y");

    coor = CLLocationCoordinate2DMake(8.0, 0.0);
    heading = -135;
    intersection = [Geo intersectionWithPolygon:polygon fromPoint:coor andHeading:heading];
    STAssertEqualsWithAccuracy(intersection.longitude, (double)FLT_MAX, FLT_EPSILON, @"From up above x");
    STAssertEqualsWithAccuracy(intersection.latitude, (double)FLT_MAX, FLT_EPSILON, @"From up above y");
    heading = 225;
    intersection = [Geo intersectionWithPolygon:polygon fromPoint:coor andHeading:heading];
    STAssertEqualsWithAccuracy(intersection.longitude, (double)FLT_MAX, FLT_EPSILON, @"From up above x");
    STAssertEqualsWithAccuracy(intersection.latitude, (double)FLT_MAX, FLT_EPSILON, @"From up above y");

    coor = CLLocationCoordinate2DMake(-4.0, 4.0);
    heading = 160.0;
    intersection = [Geo intersectionWithPolygon:polygon fromPoint:coor andHeading:heading];
    STAssertEqualsWithAccuracy(intersection.longitude, (double)FLT_MAX, FLT_EPSILON, @"Totally away x");
    STAssertEqualsWithAccuracy(intersection.latitude, (double)FLT_MAX, FLT_EPSILON, @"Totally away x");
}

@end
