//
//  DrawView.h
//  CapitalLocation
//
//  Created by Romans Karpelcevs on 1/12/13.
//
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface DrawView : UIView

- (void)drawBuildings:(NSArray *)buildings withZeroIn:(CLLocationCoordinate2D)zeroCoord;

@end
