//
//  ViewController.h
//  CapitalLocation
//
//  Created by Romans Karpelcevs on 1/9/13.
//
//

#import <UIKit/UIKit.h>
#import "LocationDatasource.h"
#import "DirectionArrow.h"
#import "DrawView.h"

@interface ViewController : UIViewController <LocationDelegate>

@property (nonatomic, strong) IBOutletCollection(DirectionArrow) NSArray *arrows;

@property (nonatomic, strong) IBOutlet UIImageView *northPoint;
@property (nonatomic, strong) IBOutlet DrawView *drawView;

@property (nonatomic, strong) IBOutlet UILabel *accuracyLabel;
@property (nonatomic, strong) IBOutlet UILabel *insideLabel;
@property (nonatomic, strong) IBOutlet UILabel *currentBuildingLabel;

@property (nonatomic, strong) LocationDatasource *locationDatasource;

- (IBAction)drawBuildings;
- (IBAction)calibrate;
- (IBAction)hideDrawView;

@end
