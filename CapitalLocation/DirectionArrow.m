//
//  DirectionArrow.m
//  CapitalLocation
//
//  Created by Romans Karpelcevs on 1/9/13.
//
//

#import <QuartzCore/QuartzCore.h>
#import "DirectionArrow.h"

@interface DirectionArrow ()

- (void)setup;

@end

@implementation DirectionArrow

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    _arrowColor = [UIColor redColor];
    _title = @"";
    self.backgroundColor = [UIColor clearColor];
    CGPoint anchorPoint = CGPointMake(0.5, 0);
    [self.layer setAnchorPoint:anchorPoint];
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSetRGBStrokeColor(context, 1.0, 0.0, 0.0, 1.0);
    CGContextSetFillColorWithColor(context, [self.arrowColor CGColor]);
    CGContextSetLineWidth(context, 1.0);

    CGContextBeginPath(context);

    CGFloat mid = self.frame.size.width / 2.0;
    CGFloat arrWidth = 4.0;
    CGFloat arrWingWidth = 8.0;
    CGFloat arrHeight = 30.0;
    CGContextMoveToPoint(context, mid, 2.0);
    CGContextAddLineToPoint(context, mid + arrWingWidth, arrHeight);
    CGContextAddLineToPoint(context, mid + arrWidth, arrHeight);
    CGContextAddLineToPoint(context, mid + arrWidth, self.frame.size.height * 2.0 / 3.0);
    CGContextAddLineToPoint(context, mid - arrWidth, self.frame.size.height * 2.0 / 3.0);
    CGContextAddLineToPoint(context, mid - arrWidth, arrHeight);
    CGContextAddLineToPoint(context, mid - arrWingWidth, arrHeight);
    CGContextAddLineToPoint(context, mid, 2.0);

    CGContextClosePath(context);

    CGContextFillPath(context);

    CGContextSetFillColorWithColor(context, [[UIColor blackColor] CGColor]);
    CGRect textRect = CGRectMake(2.0, self.frame.size.height * 2.0 / 3.0 + 2.0,
                                 self.frame.size.width - 4.0,
                                 self.frame.size.height / 3.0 - 4.0);
    CGFloat realFontSize = 16.0;
    CGSize realSize = [self.title sizeWithFont:[UIFont systemFontOfSize:realFontSize] minFontSize:10.0 actualFontSize:&realFontSize
                                      forWidth:textRect.size.width lineBreakMode:NSLineBreakByTruncatingTail];
    textRect.origin.x += (textRect.size.width - realSize.width) / 2;
    textRect.size = realSize;
    [self.title drawInRect:textRect withFont:[UIFont systemFontOfSize:realFontSize]
             lineBreakMode:NSLineBreakByTruncatingTail alignment:NSTextAlignmentCenter];
}

@end
