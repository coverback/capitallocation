//
//  BuildingObject.h
//  CapitalLocation
//
//  Created by Romans Karpelcevs on 1/10/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef enum Buildings {
    kBuildingMain = 0,
    kBuildingContainer,
    kBuildingTransformer,
    kBuildingCount
} Buildings;

@interface BuildingObject : NSObject

// Coords kept as NSValues of CLLocationCoordinate2D
@property (nonatomic, strong) NSMutableArray *coordinates;

@property (nonatomic, assign) CLLocationDirection targetHeading;
@property (nonatomic, assign) CLLocationDistance targetDistance;

- (BOOL)isPointInside:(CLLocationCoordinate2D)point;
- (void)updateAllCoordinatesWithDelta:(CLLocationCoordinate2D)delta;
- (void)calculateTargetHeadingFromLocation:(CLLocationCoordinate2D)curLocation
                                andHeading:(CLLocationDirection)heading;

@end
