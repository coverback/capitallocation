//
//  ViewController.m
//  CapitalLocation
//
//  Created by Romans Karpelcevs on 1/9/13.
//
//

#import "ViewController.h"

#import <math.h>
#import <QuartzCore/QuartzCore.h>

#import "BuildingObject.h"
#import "Geo.h"

/*
 * Needed to adjust for polar coordinates conversion.
 * Polar puts angle 0 on the East, we adjust for the North.
 */
const CGFloat kAngleOffset = -M_PI_2;

@interface ViewController ()

- (void)positionTheArrows;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setLocationDatasource:[[LocationDatasource alloc] init]];
    [self.locationDatasource setDelegate:self];

    [self setArrows:[self.arrows sortedArrayUsingComparator:^NSComparisonResult(id objA, id objB) {
        return ([objA tag] < [objB tag]) ? NSOrderedAscending  :
            ([objA tag] > [objB tag]) ? NSOrderedDescending :
            NSOrderedSame;
    }]];

    [self.arrows[kBuildingMain] setFrame:CGRectMake(160 - 50, 4, 100, 100)];
    [self.arrows[kBuildingMain] setArrowColor:[UIColor darkGrayColor]];
    [self.arrows[kBuildingMain] setTitle:@"Main building"];

    [self.arrows[kBuildingContainer] setFrame:CGRectMake(160 - 30, 4, 60, 60)];
    [self.arrows[kBuildingContainer] setArrowColor:[UIColor redColor]];
    [self.arrows[kBuildingContainer] setTitle:@"Container"];

    [self.arrows[kBuildingTransformer] setFrame:CGRectMake(160 - 40, 4, 80, 80)];
    [self.arrows[kBuildingTransformer] setArrowColor:[UIColor blueColor]];
    [self.arrows[kBuildingTransformer] setTitle:@"Transformer"];

    [self.northPoint setFrame:CGRectMake(160 - self.northPoint.frame.size.width / 2.0, 4,
                                         self.northPoint.frame.size.width, self.northPoint.frame.size.height)];
    [self.northPoint.layer setAnchorPoint:CGPointMake(0.5, 0)];
}

- (void)positionTheArrows
{
    /*
     * Position the arrows on the circle, which centre is in 160, 160 or so
     * and radius 155 or so.
     */
    CGFloat kRadius = 156.0;
    CGFloat kLeftPad = 160.0;
    CGFloat kTopPad = 160.0;

    [self.currentBuildingLabel setText:@""];
    [self.insideLabel setText:@""];

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];

    // Move on to the arrows, each should be rotated and put somewhere on the circle.
    for (int i = 0; i < kBuildingCount; i++) {
        DirectionArrow *curArrow = self.arrows[i];
        [curArrow setHidden:NO];
        CGFloat th = DEG_TO_RAD([self.locationDatasource.buildingObjects[i] targetHeading]);
        CGPoint rotationPoint = CGPointMake(kLeftPad + kRadius * cos(kAngleOffset + th),
                                            kTopPad + kRadius * sin(kAngleOffset + th));
        [[curArrow layer] setPosition:rotationPoint];
        [[curArrow layer] setTransform:CATransform3DMakeRotation(th, 0, 0, 1)];

        // Since we're iterating over arrows, test if inside.
        if ([self.locationDatasource.buildingObjects[i]
            isPointInside:[self.locationDatasource.locationManager.location coordinate]]) {
            [curArrow setHidden:YES];
            [self.insideLabel setText:[NSString stringWithFormat:@"You're inside %@!", curArrow.title]];
        }
    }

    CGFloat th = DEG_TO_RAD([self.locationDatasource relativeNorth]);
    CGPoint rotationPoint = CGPointMake(kLeftPad + kRadius * cos(kAngleOffset + th),
                                        kTopPad + kRadius * sin(kAngleOffset + th));
    [[self.northPoint layer] setPosition:rotationPoint];
    [[self.northPoint layer] setTransform:CATransform3DMakeRotation(th, 0, 0, 1)];

    [UIView commitAnimations];

    if ([self.locationDatasource currentBuilding] < kBuildingCount) {
        NSString *buildingName = [self.arrows[[self.locationDatasource currentBuilding]] title];
        [self.currentBuildingLabel setText:[NSString stringWithFormat:@"You're looking at %@!", buildingName]];
    }
}

#pragma mark - Actions

- (IBAction)drawBuildings
{
    [self.drawView drawBuildings:self.locationDatasource.buildingObjects
                      withZeroIn:self.locationDatasource.locationManager.location.coordinate];
    [self.view bringSubviewToFront:self.drawView];
    [self.drawView setHidden:NO];
}

- (IBAction)calibrate
{
    // Go over all coordinates and make correction based on current location
    [self.locationDatasource calibrateBuildings];
}

- (IBAction)hideDrawView
{
    [self.drawView setHidden:YES];
}

#pragma mark - Location data source delegate methods

- (void)updatedBuildings
{
    [self positionTheArrows];
}

- (void)updatedLocation:(CLLocation *)newLocation
{
    CLLocationAccuracy accuracy = [self.locationDatasource.locationManager.location horizontalAccuracy];
    NSString *accuracyText = [NSString stringWithFormat:@"Accuracy: %.1f m", accuracy];

    [self.accuracyLabel setText:accuracyText];
}

@end
