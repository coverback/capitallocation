//
//  DirectionArrow.h
//  CapitalLocation
//
//  Created by Romans Karpelcevs on 1/9/13.
//
//

#import <UIKit/UIKit.h>

@interface DirectionArrow : UIView

@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) UIColor *arrowColor;

@end
