//
//  AppDelegate.h
//  CapitalLocation
//
//  Created by Romans Karpelcevs on 1/9/13.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
