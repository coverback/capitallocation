//
//  DrawView.m
//  CapitalLocation
//
//  Created by Romans Karpelcevs on 1/12/13.
//
//

#import "DrawView.h"
#import "BuildingObject.h"

#import <QuartzCore/QuartzCore.h>

@implementation DrawView

- (CATextLayer *)layerWithText:(NSString *)text atPoint:(CGPoint)point
{
    CATextLayer *label = [[CATextLayer alloc] init];
    [label setFont:@"Helvetica-Bold"];
    [label setFontSize:12];
    [label setFrame:CGRectMake(point.x, point.y, 20.0, 20.0)];
    
    [label setString:text];
    [label setAlignmentMode:kCAAlignmentCenter];
    [label setForegroundColor:[[UIColor redColor] CGColor]];

    return label;
}

- (void)drawBuildings:(NSArray *)buildings withZeroIn:(CLLocationCoordinate2D)zeroCoord
{
    const CGFloat kZoomRatio = 200000.0;
    const CGPoint kCenterOffset = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);

    for (BuildingObject *building in buildings) {

        CAShapeLayer *shapeLayer = [CAShapeLayer layer];
        [shapeLayer setBounds:self.bounds];
        [shapeLayer setPosition:self.center];
        [shapeLayer setFillColor:[[UIColor clearColor] CGColor]];
        [shapeLayer setStrokeColor:[[UIColor blackColor] CGColor]];
        [shapeLayer setLineWidth:1];
        CGMutablePathRef path = CGPathCreateMutable();

        CGPoint startPoint;
        for (int i = 0; i < [building.coordinates count]; i++) {
            NSValue *val = building.coordinates[i];
            CLLocationCoordinate2D coordinate;
            [val getValue:&coordinate];

            // y is inversed when drawing
            CGFloat y = kCenterOffset.y + (coordinate.latitude - zeroCoord.latitude) * -kZoomRatio;
            CGFloat x = kCenterOffset.x + (coordinate.longitude - zeroCoord.longitude) * kZoomRatio;

            NSString *pointNo = [@(i) stringValue];
            [[self layer] addSublayer:[self layerWithText:pointNo atPoint:CGPointMake(x, y)]];

            if (i == 0) {
                startPoint = CGPointMake(x, y);
                CGPathMoveToPoint(path, NULL, x, y);
            } else {
                CGPathAddLineToPoint(path, NULL, x, y);
            }
        }
        CGPathAddLineToPoint(path, NULL, startPoint.x, startPoint.y);

        [shapeLayer setPath:path];
        CGPathRelease(path);
        [[self layer] addSublayer:shapeLayer];
    }
}

@end
