//
//  LocationDatasource.m
//  CapitalLocation
//
//  Created by Romans Karpelcevs on 1/9/13.
//
//

#import "LocationDatasource.h"
#import "BuildingObject.h"
#import "Geo.h"

@interface LocationDatasource ()

- (void)createBuildingObjects;
- (void)updateBuildingObjects;

@end

#pragma mark -

@implementation LocationDatasource

- (id)init
{
    self = [super init];
    if (self) {
        [self createBuildingObjects];

        _locationManager = [[CLLocationManager alloc] init];
        if ([CLLocationManager locationServicesEnabled]) {
            [_locationManager setDelegate:self];
            [_locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
            [_locationManager setHeadingFilter:1.0];
            [_locationManager setDistanceFilter:2.5];
            [_locationManager startUpdatingLocation];
            [_locationManager startUpdatingHeading];
        }
    }

    return self;
}

- (CLLocationDirection)relativeNorth
{
    return [Geo relativeHeadingWithCurrent:self.locationManager.heading.trueHeading andTarget:0.0];
}

#pragma mark - Building setups

- (void)createBuildingObjects
{
    NSMutableArray *buildings = [[NSMutableArray alloc] init];
    NSArray *filenames = @[@"main", @"container", @"transformer"];

    for (NSString *filename in filenames) {
        // Get the list of coordinates
        NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:filename ofType:@"plist"];
        NSArray *pointNumbers = [[NSMutableArray alloc] initWithContentsOfFile:path];

        // Prepare where to put them
        BuildingObject *building = [[BuildingObject alloc] init];
        NSMutableArray *points = [[NSMutableArray alloc] init];

        for (NSDictionary *dict in pointNumbers) {
            CLLocationCoordinate2D coords = CLLocationCoordinate2DMake([dict[@"latitude"] doubleValue],
                                                                       [dict[@"longitude"] doubleValue]);
            [points addObject:[NSValue valueWithBytes:&coords objCType:@encode(CLLocationCoordinate2D)]];
        }
        [building setCoordinates:points];
        [buildings addObject:building];
    }

    [self setBuildingObjects:buildings];
}

- (void)calibrateBuildings
{
    /*
     * Take current position and translate building coordinates so that they are around us.
     * Useful for calibration or testing.
     */

    CLLocationCoordinate2D location = [self.locationManager.location coordinate];

    NSValue *originVal = [self.buildingObjects[kBuildingMain] coordinates][4];
    CLLocationCoordinate2D origin;
    [originVal getValue:&origin];

    // Let's say we're a few meters to the East
    origin.longitude += 0.000015;
    CLLocationCoordinate2D delta = { location.latitude - origin.latitude, location.longitude - origin.longitude };
    for (BuildingObject *object in self.buildingObjects) {
        [object updateAllCoordinatesWithDelta:delta];
    }
    [self updateBuildingObjects];
}

- (void)updateBuildingObjects
{
    CLLocationCoordinate2D closestIntersection = { FLT_MAX, FLT_MAX };
    [self setCurrentBuilding:kBuildingCount];

    for (int i = 0; i < [self.buildingObjects count]; i++) {
        BuildingObject *object = self.buildingObjects[i];
        [object calculateTargetHeadingFromLocation:self.locationManager.location.coordinate
                                        andHeading:self.locationManager.heading.trueHeading];

        if (eq_double([object targetHeading], 0.0)) {
            CLLocationCoordinate2D intersection = [Geo intersectionWithPolygon:[object coordinates]
                                                                     fromPoint:[self.locationManager.location coordinate]
                                                                    andHeading:[self.locationManager.heading trueHeading]];
            if ([Geo relativeDistanceFrom:intersection to:[self.locationManager.location coordinate]] <
                [Geo relativeDistanceFrom:closestIntersection to:[self.locationManager.location coordinate]]) {
                closestIntersection = intersection;
                [self setCurrentBuilding:i];
            }
        }
    }
    [self.delegate updatedBuildings];
}


#pragma mark - Delegate methods

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    [self updateBuildingObjects];
    if ([self.delegate respondsToSelector:@selector(updatedHeading:)]) {
        [self.delegate updatedHeading:newHeading];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [self updateBuildingObjects];
    if ([self.delegate respondsToSelector:@selector(updatedLocation:)]) {
        [self.delegate updatedLocation:[locations lastObject]];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    [self updateBuildingObjects];
    if ([self.delegate respondsToSelector:@selector(updatedLocation:)]) {
        [self.delegate updatedLocation:newLocation];
    }
}

@end
