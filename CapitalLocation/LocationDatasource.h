//
//  LocationDatasource.h
//  CapitalLocation
//
//  Created by Romans Karpelcevs on 1/9/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol LocationDelegate <NSObject>

@required
- (void)updatedBuildings;

@optional
- (void)updatedLocation:(CLLocation *)newLocation;
- (void)updatedHeading:(CLHeading *)newHeading;

@end

@interface LocationDatasource : NSObject <CLLocationManagerDelegate>

@property (nonatomic, weak) id<LocationDelegate> delegate;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, readonly) CLLocationDirection relativeNorth;
@property (nonatomic, assign) int currentBuilding;

@property (nonatomic, strong) NSArray *buildingObjects;

- (void)calibrateBuildings;

@end
