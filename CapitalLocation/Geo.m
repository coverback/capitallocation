//
//  Geo.m
//  CapitalLocation
//
//  Created by Romans Karpelcevs on 1/11/13.
//
//

#import "Geo.h"

static inline void swap(double *num1, double *num2)
{
    double tmp = *num1;
    *num1 = *num2;
    *num2 = tmp;
}

inline CLLine CLLineMake(double x1, double y1, double x2, double y2)
{
    CLLine l = { x1, y1, x2, y2 };
    return l;
}

inline CLLine CLLineMakeC(CLLocationCoordinate2D p1, CLLocationCoordinate2D p2)
{
    CLLine l;
    l.x1 = p1.longitude;
    l.y1 = p1.latitude;
    l.x2 = p2.longitude;
    l.y2 = p2.latitude;
    return l;
}

static inline double vector_length(double delta_x, double delta_y)
{
    return sqrt(delta_x * delta_x + delta_y * delta_y);
}

inline bool eq_double(double x, double y)
{
    return (fabs(x - y) < 1000 * DBL_EPSILON * fabs(x + y) || fabs(x - y) < 100 * DBL_EPSILON);
}


@implementation Geo

+ (CLLocationDirection)absoluteHeadingFrom:(CLLocationCoordinate2D)currentLocation
                                        to:(CLLocationCoordinate2D)targetLocation
{
    // x that we're used to is longitude
    double x = targetLocation.longitude - currentLocation.longitude;
    double y = targetLocation.latitude - currentLocation.latitude;
    double th = atan2(x, y);
    // Convert [-pi; pi] to [0, 2*pi)
    if (th < 0)
        th += M_PI * 2;

    return RAD_TO_DEG(th);
}

+ (CLLocationDirection)relativeHeadingWithCurrent:(CLLocationDirection)currentHeading
                                        andTarget:(CLLocationDirection)targetHeading
{
    CLLocationDirection result = fmod(targetHeading - currentHeading, 360.0);
    // Convert from whatever to (-180, 180]
    if (result > 180.0)
        result -= 360.0;
    if (result <= -180.0)
        result += 360.0;
    return result;
}

+ (BOOL)isPoint:(CLLocationCoordinate2D)point insideClosedPath:(NSArray *)coordinates
{
    /*
     * Create a path and use Apple's function for this.
     * It will use a ray crossing algorithm.
     */
    CGMutablePathRef path = CGPathCreateMutable();
    CGPoint startPoint;
    for (int i = 0; i < [coordinates count]; i++) {
        NSValue *val = coordinates[i];
        CLLocationCoordinate2D coordinate;
        [val getValue:&coordinate];

        CGFloat y = coordinate.latitude;
        CGFloat x = coordinate.longitude;

        if (i == 0) {
            startPoint = CGPointMake(x, y);
            CGPathMoveToPoint(path, NULL, x, y);
        } else {
            CGPathAddLineToPoint(path, NULL, x, y);
        }
    }
    CGPathAddLineToPoint(path, NULL, startPoint.x, startPoint.y);
    BOOL contains = CGPathContainsPoint(path, NULL, CGPointMake(point.longitude, point.latitude), true);
    CGPathRelease(path);
    return contains;
}

+ (CLLocationCoordinate2D)intersectionWithPolygon:(NSArray *)coordinates
                                        fromPoint:(CLLocationCoordinate2D)point
                                       andHeading:(CLLocationDirection)heading
{
    // Don't want it overflow on squares, so default to float max
    CLLocationCoordinate2D closestIntersection = { FLT_MAX, FLT_MAX };

    double r = 10; // Any value would do, needed to construct a second point
    CLLocationCoordinate2D point2 = CLLocationCoordinate2DMake(r * cos(DEG_TO_RAD(heading)),
                                                               r * sin(DEG_TO_RAD(heading)));
    point2.latitude += point.latitude;
    point2.longitude += point.longitude;
    CLLine userLine = CLLineMakeC(point, point2);
    /*
     * Define a block to test if an intersection is in the same direction
     * as a user, not simply on his line.
     */
    BOOL (^onUserRay)(CLLocationCoordinate2D intersection) = ^BOOL(CLLocationCoordinate2D intersection) {
        // We have "user's line", which is a vector.
        double x1 = userLine.x2 - userLine.x1;
        double y1 = userLine.y2 - userLine.y1;
        // Now construct a vector to a found point.
        double x2 = intersection.longitude - userLine.x1;
        double y2 = intersection.latitude - userLine.y1;
        // Dot product's sign will be our clue.
        double dotProduct = x1 * x2 + y1 * y2;
        return dotProduct > 0;
    };

    for (int i = 0; i < [coordinates count]; i++) {
        NSValue *val = coordinates[i];
        CLLocationCoordinate2D coordinate;
        [val getValue:&coordinate];

        double y1 = coordinate.latitude;
        double x1 = coordinate.longitude;

        if (i < [coordinates count] - 1)
            val = coordinates[i + 1];
        else
            val = coordinates[0];
        [val getValue:&coordinate];

        double y2 = coordinate.latitude;
        double x2 = coordinate.longitude;

        CLLocationCoordinate2D intersection = [self intersectionBetween:CLLineMake(x1, y1, x2, y2) and:userLine];

        // For easier comparisons, make these limits aligned so that x1 < x2
        if (x1 > x2)
            swap(&x1, &x2);
        if (y1 > y2)
            swap(&y1, &y2);

        /*
         * Line intersection won't do, we need to see that the intersection
         * happened in needed segment. Simple x check won't do because of the
         * lines/segments with constanct x, so check y as well.
         * Even this isn't enough, as only intersection in front of the user
         * are valid.
         */
        if (intersection.longitude >= x1 && intersection.longitude <= x2
            && intersection.latitude >= y1 && intersection.latitude <= y2
            && onUserRay(intersection)) {
            // This is real intersection. Find shortest.
            if ([self relativeDistanceFrom:intersection to:point] <
                [self relativeDistanceFrom:closestIntersection to:point])
                closestIntersection = intersection;
        }
    }

    return closestIntersection;
}

+ (CLLocationCoordinate2D)intersectionBetween:(CLLine)line1 and:(CLLine)line2
{
    CLLocationCoordinate2D intersection;
    // For shorter formulae
    double x1 = line1.x1, x2 = line1.x2, x3 = line2.x1, x4 = line2.x2;
    double y1 = line1.y1, y2 = line1.y2, y3 = line2.y1, y4 = line2.y2;

    // This is just taken off Wikipedia, but it's fairly basic
    double divisor = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
    if (eq_double(divisor, 0.0)) {
        intersection = CLLocationCoordinate2DMake(FLT_MAX, FLT_MAX);
    } else {
        double divident_x = (x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4);
        double divident_y = (x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4);
        intersection.longitude = divident_x / divisor;
        intersection.latitude = divident_y / divisor;
    }
    return intersection;
}

+ (CLLocationDistance)relativeDistanceFrom:(CLLocationCoordinate2D)loc1 to:(CLLocationCoordinate2D)loc2
{
    double delta_x = loc1.longitude - loc2.longitude;
    double delta_y = loc1.latitude - loc2.latitude;
    return vector_length(delta_x, delta_y);
}

@end
