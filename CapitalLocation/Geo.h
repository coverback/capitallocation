//
//  Geo.h
//  CapitalLocation
//
//  Created by Romans Karpelcevs on 1/11/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#define DEG_TO_RAD(x) ((x) * (M_PI / 180))
#define RAD_TO_DEG(x) ((x) * (180 / M_PI))

typedef struct CLLine {
    double x1, y1;
    double x2, y2;
} CLLine;

CLLine CLLineMake(double x1, double y1, double x2, double y2);
CLLine CLLineMakeC(CLLocationCoordinate2D p1, CLLocationCoordinate2D p2);

bool eq_double(double x, double y);

@interface Geo : NSObject

+ (CLLocationDirection)absoluteHeadingFrom:(CLLocationCoordinate2D)currentLocation
                                        to:(CLLocationCoordinate2D)targetLocation;

+ (CLLocationDirection)relativeHeadingWithCurrent:(CLLocationDirection)currentHeading
                                        andTarget:(CLLocationDirection)targetHeading;

+ (BOOL)isPoint:(CLLocationCoordinate2D)point insideClosedPath:(NSArray *)coordinates;

+ (CLLocationCoordinate2D)intersectionWithPolygon:(NSArray *)coordinates
                                        fromPoint:(CLLocationCoordinate2D)point
                                       andHeading:(CLLocationDirection)heading;

+ (CLLocationCoordinate2D)intersectionBetween:(CLLine)line1 and:(CLLine)line2;

+ (CLLocationDistance)relativeDistanceFrom:(CLLocationCoordinate2D)point1 to:(CLLocationCoordinate2D)point2;


@end
