//
//  BuildingObject.m
//  CapitalLocation
//
//  Created by Romans Karpelcevs on 1/10/13.
//
//

#import "BuildingObject.h"

#import <math.h>

#import "Geo.h"

@implementation BuildingObject

- (BOOL)isPointInside:(CLLocationCoordinate2D)point
{
    return [Geo isPoint:point insideClosedPath:self.coordinates];
}

- (void)updateAllCoordinatesWithDelta:(CLLocationCoordinate2D)delta
{
    /*
     * Go through all coordinates and adjust them by needed
     * delta. Needed in case saved coordinates need and offset-only
     * correct. Especially good for outdoor testing.
     */
    for (int i = 0; i < [self.coordinates count]; i++) {
        NSValue *val = self.coordinates[i];
        CLLocationCoordinate2D coordinate;
        [val getValue:&coordinate];

        coordinate.latitude += delta.latitude;
        coordinate.longitude += delta.longitude;

        val = [NSValue valueWithBytes:&coordinate objCType:@encode(CLLocationCoordinate2D)];
        self.coordinates[i] = val;
    }
}

- (void)calculateTargetHeadingFromLocation:(CLLocationCoordinate2D)curLocation
                                andHeading:(CLLocationDirection)heading
{
    // Used to filter down closest point
    CLLocationDirection target = 180.0;

    CLLocationCoordinate2D intersection = [Geo intersectionWithPolygon:self.coordinates
                                                             fromPoint:curLocation andHeading:heading];
    BOOL inFront = (intersection.latitude < FLT_MAX && intersection.longitude < FLT_MAX);

    if (inFront) {
        /*
         * If our course intersects with the building's polygon
         * set target to 0 and stop calculations.
         */
        target = 0.0;
    } else {
        /*
         * Otherwise, go over all dots and find closest angle-wise.
         */
        for (NSValue *val in self.coordinates) {
            CLLocationCoordinate2D coordinate;
            [val getValue:&coordinate];

            double th = [Geo absoluteHeadingFrom:curLocation to:coordinate];
            CLLocationDirection t = [Geo relativeHeadingWithCurrent:heading andTarget:th];

            // Now assign/update closest and two edge point headings
            if (fabs(t) < fabs(target))
                target = t;
        }
    }

    [self setTargetHeading:target];
}

@end
